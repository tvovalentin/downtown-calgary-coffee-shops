# Downtown Calgary Coffee Shops

## Python Dependencies

pip install dash
pip install folium
jobspy


### Build Docker Image
docker build -t coffee-shops-img .

### Run Docker Container
sudo docker-compose up -d


## SSL Certificates

sudo openssl req -newkey rsa:4096 \
            -x509 \
            -sha256 \
            -days 3650 \
            -nodes \
            -out coffee.crt \
            -keyout coffee.key
