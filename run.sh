#!/bin/bash

# Build Docker Image
docker build -t coffee-shops-img .

# Run Docker Container
sudo docker-compose up -d