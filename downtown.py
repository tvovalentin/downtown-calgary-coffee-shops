# import dash
import folium
# from jobs import app
# import dash_core_components as dcc
# import dash_html_components as html
from dash.dependencies import Input, Output

from dash import Dash, dcc, html, Input, Output
import os

from coffee_shops.you_and_i import YouAndI
from coffee_shops.artigiano import Artigiano
from coffee_shops.starbucks import Starbucks
from coffee_shops.tim_hortons import TimHortons
from coffee_shops.second_cup import SecondCup

from coffee_shops.hutch import Hutch
from coffee_shops.alforno import Alforno


MAP_ALL = 'map_all.html'
MAP_PLUS_15 = 'map_plus_15.html'
MAP_OUTDOORS = 'map_outdoors.html'

all = folium.Map(location = [51.051, -114.070], zoom_start = 16)
plus_15 = folium.Map(location = [51.051, -114.070], zoom_start = 16)
outdoors = folium.Map(location = [51.051, -114.070], zoom_start = 16)

# Helcim
helcim_loc = [51.0518, -114.071] # refactor
helcim_marker = folium.Marker(location = helcim_loc, popup = 'Helcim')


# Hutch
coffee_shops_plus_15 = [
    Starbucks(),
    Artigiano(),
    TimHortons(),
    SecondCup(),
    YouAndI()
]

coffee_shops_outdoors = [
    Alforno(),
    Hutch()
]

coffee_shops_all = coffee_shops_plus_15 + coffee_shops_outdoors

# Maps
def add_to_map(map_name, coffee_shops):
    for coffee_shop in coffee_shops:
        coffee_shop.marker.add_to(map_name)
        for path in coffee_shop.paths:
            path.add_to(map_name)


add_to_map(plus_15, coffee_shops_plus_15)
helcim_marker.add_to(plus_15) # why here?
plus_15.save(MAP_PLUS_15)

add_to_map(outdoors, coffee_shops_outdoors)
helcim_marker.add_to(outdoors) # why here?
outdoors.save(MAP_OUTDOORS)

add_to_map(all, coffee_shops_all)
helcim_marker.add_to(all) # why here?
all.save(MAP_ALL)



external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div([
    html.H1('Downtown Calgary - Coffee Shops'),
    html.Div([
        html.Span("Option: "),
        dcc.Dropdown(['All', '+ 15', 'Outdoors'], id = 'options', value = "All")
    ]),
    html.Iframe(id = 'map', srcDoc = open(MAP_ALL, 'r').read(), width='100%', height = '600')
])



@app.callback(
    Output("map", "srcDoc"), 
    [Input("options", "value")]
)
def update_fig(input_value):
    if input_value == '+ 15':
        return open(MAP_PLUS_15, 'r').read()
    if input_value == 'Outdoors':
        return open(MAP_OUTDOORS, 'r').read()
    return open(MAP_ALL, 'r').read()

if __name__ == '__main__':
    # context = ('coffee.crt','coffee.key')
    # app.run_server(host='0.0.0.0', debug=True, ssl_context=context)
    app.run_server(host='0.0.0.0', debug=True)