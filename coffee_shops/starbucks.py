import folium


class Starbucks:

    loc = [51.04983, -114.06971]

    popup = '''
        <h4 style="text-align: center;">Starbucks</h4> 
        <hr/>

        <div style="display: flex; justify-content: space-between;">
            <span style="margin-right:20px;">
                <img src="assets/open.png" width="20" height="20">
                <span style="font-size: 15px;">6:30am-3pm</span>
            </span>

            <span style="margin-left:20px;">
                <img src="assets/walk.png" width="20" height="20">
                <span style="font-size: 15px;">8'</span>
            </span>
        </div>
    '''

    marker = folium.Marker(
        location = loc, 
        popup = folium.Popup(folium.Html(popup, script = True), max_width = 500), 
        icon = folium.Icon(color = 'pink', icon = 'glyphicon')
    )

    paths = [
        folium.PolyLine(
            [
                [51.0518, -114.071], # refactor
                [51.05172, -114.07100],
                [51.05171, -114.07073],
                [51.05116, -114.07080],
                [51.05105, -114.07100],
                [51.05092, -114.07078],
                [51.05084, -114.07090],
                [51.05081, -114.07085],
                [51.05067, -114.07085],
                [51.05066, -114.07071],
                [51.05024, -114.07073],
                [51.05021, -114.07069],
                [51.05020, -114.07062],
                [51.05020, -114.07055],
                [51.05003, -114.07055],
                [51.05001, -114.06997]
            ],
            color = 'pink',
            weight = 5,
            opacity = 0.9
        ),
        folium.PolyLine(
            [
                [51.05001, -114.06997],
                [51.04997, -114.06976],
                [51.04997, -114.06970],
                loc
            ],
            color = 'pink',
            weight = 5,
            opacity = 0.5
        )
    ]
