import folium


class TimHortons:

    loc = [51.05107, -114.07270]

    popup = '''
        <h4 style="text-align: center;">Tim Hortons</h4> 
        <hr/>

        <div style="display: flex; justify-content: space-between;">
            <span style="margin-right:20px;">
                <img src="assets/open.png" width="20" height="20">
                <span style="font-size: 15px;">7am-2pm</span>
            </span>

            <span style="margin-left:20px;">
                <img src="assets/walk.png" width="20" height="20">
                <span style="font-size: 15px;">4'</span>
            </span>
        </div>
    '''

    marker = folium.Marker(
        location = loc, 
        popup = folium.Popup(folium.Html(popup, script = True), max_width = 500), 
        icon = folium.Icon(color = 'lightred', icon = 'glyphicon')
    )

    paths = [
        folium.PolyLine(
            [
                [51.0518, -114.071], # refactor
                [51.05172, -114.07100],
                [51.05171, -114.07073],
                [51.05116, -114.07080],
                [51.05098, -114.07112],
                [51.05102, -114.07115],
                loc
            ],
            color = 'red',
            weight = 5,
            opacity = 0.3
        )
    ]
