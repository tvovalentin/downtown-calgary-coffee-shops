import folium


class Alforno:

    loc = [51.05142, -114.07814]

    popup = '''
        <h4 style="text-align: center;">Alforno</h4> 
        <hr/>

        <div style="display: flex; justify-content: space-between;">
            <span style="margin-right:20px;">
                <img src="assets/open.png" width="20" height="20">
                <span style="font-size: 15px;">8am-9pm</span>
            </span>

            <span style="margin-left:20px;">
                <img src="assets/walk.png" width="20" height="20">
                <span style="font-size: 15px;">7'</span>
            </span>
        </div>
    '''

    marker = folium.Marker(
        location = loc, 
        popup = folium.Popup(folium.Html(popup, script = True), max_width = 500), 
        icon = folium.Icon(color = 'orange', icon = 'glyphicon')
    )

    paths = [
        folium.PolyLine(
            [
                [51.0518, -114.071], # refactor
                [51.05172, -114.07100],
                [51.05172, -114.07110],
                [51.05150, -114.07110],
                [51.05165, -114.07598],
                [51.05171, -114.07835],
                [51.05142, -114.07836],
                loc
            ],
            color = 'orange',
            weight = 5,
            opacity = 0.6
        )
    ]
