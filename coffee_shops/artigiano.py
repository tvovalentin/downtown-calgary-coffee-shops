import folium


class Artigiano:

    loc = [51.05006, -114.07050]

    popup = '''
        <h4 style="text-align: center;">Artigiano</h4> 
        <hr/>

        <div style="display: flex; justify-content: space-between;">
            <span style="margin-right:20px;">
                <img src="assets/open.png" width="20" height="20">
                <span style="font-size: 15px;">7am-4pm</span>
            </span>

            <span style="margin-left:20px;">
                <img src="assets/walk.png" width="20" height="20">
                <span style="font-size: 15px;">7'</span>
            </span>
        </div>
    '''

    marker = folium.Marker(
        location = loc, 
        popup = folium.Popup(folium.Html(popup, script = True), max_width = 500), 
        icon = folium.Icon(color = 'green', icon = 'glyphicon')
    )

    paths = [
        folium.PolyLine(
            [
                [51.0518, -114.071], # refactor
                [51.05172, -114.07100],
                [51.05171, -114.07073],
                [51.05116, -114.07080],
                [51.05105, -114.07100],
                [51.05092, -114.07078],
                [51.05084, -114.07090],
                [51.05081, -114.07085],
                [51.05067, -114.07085],
                [51.05066, -114.07071],
                [51.05024, -114.07073],
                [51.05021, -114.07069],
                [51.05020, -114.07062],
                [51.05020, -114.07055],
                [51.05000, -114.07055],
                [51.05000, -114.07053]
            ],
            color = 'green',
            weight = 5,
            opacity = 0.8
        ),
        folium.PolyLine(
            [
                [51.05000, -114.07053],
                [51.04996, -114.07053],
                [51.04996, -114.07049],
                [51.05003, -114.07049],
                [51.05003, -114.07051],
                loc
            ],
            color = 'green',
            weight = 5,
            opacity = 0.4
        )
    ]
