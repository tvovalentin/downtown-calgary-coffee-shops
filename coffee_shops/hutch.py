import folium


class Hutch:

    loc = [51.05208, -114.07808]

    popup = '''
        <h4 style="text-align: center;">Hutch</h4> 
        <hr/>

        <div style="display: flex; justify-content: space-between;">
            <span style="margin-right:20px;">
                <img src="assets/open.png" width="20" height="20">
                <span style="font-size: 15px;">8am-4pm</span>
            </span>

            <span style="margin-left:20px;">
                <img src="assets/walk.png" width="20" height="20">
                <span style="font-size: 15px;">8'</span>
            </span>
        </div>
    '''

    marker = folium.Marker(
        location = loc, 
        popup = folium.Popup(folium.Html(popup, script = True), max_width = 500), 
        icon = folium.Icon(color = 'blue', icon = 'glyphicon')
    )

    paths = [
        folium.PolyLine(
            [
                [51.0518, -114.071], # refactor
                [51.05172, -114.07100],
                [51.05172, -114.07110],
                [51.05150, -114.07110],
                [51.05165, -114.07598],
                [51.05226, -114.07596],
                [51.05226, -114.07808],
                loc
            ],
            color = 'blue',
            weight = 5,
            opacity = 0.6
        )
    ]
