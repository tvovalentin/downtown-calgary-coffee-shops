# set base image (host OS) - Ubuntu
FROM python:3.7

# set the working directory in the container
WORKDIR /coffee

# install dependencies
RUN pip install --upgrade pip
RUN pip install dash
RUN pip install folium

# command to run on container start
CMD [ "python3", "./downtown.py" ]